ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Manifest Merging:
-----------------
Your project uses libraries that provide manifests, and your Eclipse
project did not explicitly turn on manifest merging. In Android Gradle
projects, manifests are always merged (meaning that contents from your
libraries' manifests will be merged into the app manifest. If you had
manually copied contents from library manifests into your app manifest
you may need to remove these for the app to build correctly.

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

From OpenCV Library - 3.0.0:
* javadoc/
* javadoc/allclasses-frame.html
* javadoc/allclasses-noframe.html
* javadoc/constant-values.html
* javadoc/help-doc.html
* javadoc/index-all.html
* javadoc/index.html
* javadoc/org/
* javadoc/org/opencv/
* javadoc/org/opencv/android/
* javadoc/org/opencv/android/BaseLoaderCallback.html
* javadoc/org/opencv/android/CameraBridgeViewBase.CvCameraViewFrame.html
* javadoc/org/opencv/android/CameraBridgeViewBase.CvCameraViewListener.html
* javadoc/org/opencv/android/CameraBridgeViewBase.CvCameraViewListener2.html
* javadoc/org/opencv/android/CameraBridgeViewBase.ListItemAccessor.html
* javadoc/org/opencv/android/CameraBridgeViewBase.html
* javadoc/org/opencv/android/FpsMeter.html
* javadoc/org/opencv/android/InstallCallbackInterface.html
* javadoc/org/opencv/android/JavaCameraView.JavaCameraSizeAccessor.html
* javadoc/org/opencv/android/JavaCameraView.html
* javadoc/org/opencv/android/LoaderCallbackInterface.html
* javadoc/org/opencv/android/OpenCVLoader.html
* javadoc/org/opencv/android/Utils.html
* javadoc/org/opencv/android/package-frame.html
* javadoc/org/opencv/android/package-summary.html
* javadoc/org/opencv/android/package-tree.html
* javadoc/org/opencv/calib3d/
* javadoc/org/opencv/calib3d/Calib3d.html
* javadoc/org/opencv/calib3d/StereoBM.html
* javadoc/org/opencv/calib3d/StereoMatcher.html
* javadoc/org/opencv/calib3d/StereoSGBM.html
* javadoc/org/opencv/calib3d/package-frame.html
* javadoc/org/opencv/calib3d/package-summary.html
* javadoc/org/opencv/calib3d/package-tree.html
* javadoc/org/opencv/core/
* javadoc/org/opencv/core/Algorithm.html
* javadoc/org/opencv/core/Core.MinMaxLocResult.html
* javadoc/org/opencv/core/Core.html
* javadoc/org/opencv/core/CvException.html
* javadoc/org/opencv/core/CvType.html
* javadoc/org/opencv/core/DMatch.html
* javadoc/org/opencv/core/KeyPoint.html
* javadoc/org/opencv/core/Mat.html
* javadoc/org/opencv/core/MatOfByte.html
* javadoc/org/opencv/core/MatOfDMatch.html
* javadoc/org/opencv/core/MatOfDouble.html
* javadoc/org/opencv/core/MatOfFloat.html
* javadoc/org/opencv/core/MatOfFloat4.html
* javadoc/org/opencv/core/MatOfFloat6.html
* javadoc/org/opencv/core/MatOfInt.html
* javadoc/org/opencv/core/MatOfInt4.html
* javadoc/org/opencv/core/MatOfKeyPoint.html
* javadoc/org/opencv/core/MatOfPoint.html
* javadoc/org/opencv/core/MatOfPoint2f.html
* javadoc/org/opencv/core/MatOfPoint3.html
* javadoc/org/opencv/core/MatOfPoint3f.html
* javadoc/org/opencv/core/MatOfRect.html
* javadoc/org/opencv/core/Point.html
* javadoc/org/opencv/core/Point3.html
* javadoc/org/opencv/core/Range.html
* javadoc/org/opencv/core/Rect.html
* javadoc/org/opencv/core/RotatedRect.html
* javadoc/org/opencv/core/Scalar.html
* javadoc/org/opencv/core/Size.html
* javadoc/org/opencv/core/TermCriteria.html
* javadoc/org/opencv/core/package-frame.html
* javadoc/org/opencv/core/package-summary.html
* javadoc/org/opencv/core/package-tree.html
* javadoc/org/opencv/features2d/
* javadoc/org/opencv/features2d/DescriptorExtractor.html
* javadoc/org/opencv/features2d/DescriptorMatcher.html
* javadoc/org/opencv/features2d/FeatureDetector.html
* javadoc/org/opencv/features2d/Features2d.html
* javadoc/org/opencv/features2d/package-frame.html
* javadoc/org/opencv/features2d/package-summary.html
* javadoc/org/opencv/features2d/package-tree.html
* javadoc/org/opencv/imgproc/
* javadoc/org/opencv/imgproc/CLAHE.html
* javadoc/org/opencv/imgproc/Imgproc.html
* javadoc/org/opencv/imgproc/LineSegmentDetector.html
* javadoc/org/opencv/imgproc/Subdiv2D.html
* javadoc/org/opencv/imgproc/package-frame.html
* javadoc/org/opencv/imgproc/package-summary.html
* javadoc/org/opencv/imgproc/package-tree.html
* javadoc/org/opencv/ml/
* javadoc/org/opencv/ml/ANN_MLP.html
* javadoc/org/opencv/ml/Boost.html
* javadoc/org/opencv/ml/DTrees.html
* javadoc/org/opencv/ml/EM.html
* javadoc/org/opencv/ml/KNearest.html
* javadoc/org/opencv/ml/LogisticRegression.html
* javadoc/org/opencv/ml/Ml.html
* javadoc/org/opencv/ml/NormalBayesClassifier.html
* javadoc/org/opencv/ml/RTrees.html
* javadoc/org/opencv/ml/SVM.html
* javadoc/org/opencv/ml/StatModel.html
* javadoc/org/opencv/ml/TrainData.html
* javadoc/org/opencv/ml/package-frame.html
* javadoc/org/opencv/ml/package-summary.html
* javadoc/org/opencv/ml/package-tree.html
* javadoc/org/opencv/objdetect/
* javadoc/org/opencv/objdetect/BaseCascadeClassifier.html
* javadoc/org/opencv/objdetect/CascadeClassifier.html
* javadoc/org/opencv/objdetect/HOGDescriptor.html
* javadoc/org/opencv/objdetect/Objdetect.html
* javadoc/org/opencv/objdetect/package-frame.html
* javadoc/org/opencv/objdetect/package-summary.html
* javadoc/org/opencv/objdetect/package-tree.html
* javadoc/org/opencv/photo/
* javadoc/org/opencv/photo/AlignExposures.html
* javadoc/org/opencv/photo/AlignMTB.html
* javadoc/org/opencv/photo/CalibrateCRF.html
* javadoc/org/opencv/photo/CalibrateDebevec.html
* javadoc/org/opencv/photo/CalibrateRobertson.html
* javadoc/org/opencv/photo/MergeDebevec.html
* javadoc/org/opencv/photo/MergeExposures.html
* javadoc/org/opencv/photo/MergeMertens.html
* javadoc/org/opencv/photo/MergeRobertson.html
* javadoc/org/opencv/photo/Photo.html
* javadoc/org/opencv/photo/Tonemap.html
* javadoc/org/opencv/photo/TonemapDrago.html
* javadoc/org/opencv/photo/TonemapDurand.html
* javadoc/org/opencv/photo/TonemapMantiuk.html
* javadoc/org/opencv/photo/TonemapReinhard.html
* javadoc/org/opencv/photo/package-frame.html
* javadoc/org/opencv/photo/package-summary.html
* javadoc/org/opencv/photo/package-tree.html
* javadoc/org/opencv/utils/
* javadoc/org/opencv/utils/Converters.html
* javadoc/org/opencv/utils/package-frame.html
* javadoc/org/opencv/utils/package-summary.html
* javadoc/org/opencv/utils/package-tree.html
* javadoc/org/opencv/video/
* javadoc/org/opencv/video/BackgroundSubtractor.html
* javadoc/org/opencv/video/BackgroundSubtractorKNN.html
* javadoc/org/opencv/video/BackgroundSubtractorMOG2.html
* javadoc/org/opencv/video/DenseOpticalFlow.html
* javadoc/org/opencv/video/DualTVL1OpticalFlow.html
* javadoc/org/opencv/video/KalmanFilter.html
* javadoc/org/opencv/video/Video.html
* javadoc/org/opencv/video/package-frame.html
* javadoc/org/opencv/video/package-summary.html
* javadoc/org/opencv/video/package-tree.html
* javadoc/overview-frame.html
* javadoc/overview-summary.html
* javadoc/overview-tree.html
* javadoc/package-list
* javadoc/resources/
* javadoc/resources/inherit.gif
* javadoc/serialized-form.html
* javadoc/stylesheet.css
From face-recognition:
* .externalToolBuilders/
* .externalToolBuilders/org.eclipse.cdt.managedbuilder.core.genmakebuilder.launch
* README.md
* dd.cproject
* dd.project
* icgreenweb.png
* icredweb.png
* icyellowweb.png
* project.properties~

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

In OpenCV Library - 3.0.0:
* AndroidManifest.xml => openCVLibrary300/src/main/AndroidManifest.xml
* lint.xml => openCVLibrary300/lint.xml
* res/ => openCVLibrary300/src/main/res/
* src/ => openCVLibrary300/src/main/java/
* src/org/opencv/engine/OpenCVEngineInterface.aidl => openCVLibrary300/src/main/aidl/org/opencv/engine/OpenCVEngineInterface.aidl
In face-recognition:
* AndroidManifest.xml => facerecognition/src/main/AndroidManifest.xml
* assets/ => facerecognition/src/main/assets
* libs/armeabi-v7a/libdetection_based_tracker.so => facerecognition/src/main/jniLibs/armeabi-v7a/libdetection_based_tracker.so
* libs/armeabi-v7a/libjniARToolKitPlus.so => facerecognition/src/main/jniLibs/armeabi-v7a/libjniARToolKitPlus.so
* libs/armeabi-v7a/libjniavcodec.so => facerecognition/src/main/jniLibs/armeabi-v7a/libjniavcodec.so
* libs/armeabi-v7a/libjniavdevice.so => facerecognition/src/main/jniLibs/armeabi-v7a/libjniavdevice.so
* libs/armeabi-v7a/libjniavfilter.so => facerecognition/src/main/jniLibs/armeabi-v7a/libjniavfilter.so
* libs/armeabi-v7a/libjniavformat.so => facerecognition/src/main/jniLibs/armeabi-v7a/libjniavformat.so
* libs/armeabi-v7a/libjniavutil.so => facerecognition/src/main/jniLibs/armeabi-v7a/libjniavutil.so
* libs/armeabi-v7a/libjnicvkernels.so => facerecognition/src/main/jniLibs/armeabi-v7a/libjnicvkernels.so
* libs/armeabi-v7a/libjniopencv_calib3d.so => facerecognition/src/main/jniLibs/armeabi-v7a/libjniopencv_calib3d.so
* libs/armeabi-v7a/libjniopencv_contrib.so => facerecognition/src/main/jniLibs/armeabi-v7a/libjniopencv_contrib.so
* libs/armeabi-v7a/libjniopencv_core.so => facerecognition/src/main/jniLibs/armeabi-v7a/libjniopencv_core.so
* libs/armeabi-v7a/libjniopencv_features2d.so => facerecognition/src/main/jniLibs/armeabi-v7a/libjniopencv_features2d.so
* libs/armeabi-v7a/libjniopencv_flann.so => facerecognition/src/main/jniLibs/armeabi-v7a/libjniopencv_flann.so
* libs/armeabi-v7a/libjniopencv_highgui.so => facerecognition/src/main/jniLibs/armeabi-v7a/libjniopencv_highgui.so
* libs/armeabi-v7a/libjniopencv_imgproc.so => facerecognition/src/main/jniLibs/armeabi-v7a/libjniopencv_imgproc.so
* libs/armeabi-v7a/libjniopencv_legacy.so => facerecognition/src/main/jniLibs/armeabi-v7a/libjniopencv_legacy.so
* libs/armeabi-v7a/libjniopencv_ml.so => facerecognition/src/main/jniLibs/armeabi-v7a/libjniopencv_ml.so
* libs/armeabi-v7a/libjniopencv_nonfree.so => facerecognition/src/main/jniLibs/armeabi-v7a/libjniopencv_nonfree.so
* libs/armeabi-v7a/libjniopencv_objdetect.so => facerecognition/src/main/jniLibs/armeabi-v7a/libjniopencv_objdetect.so
* libs/armeabi-v7a/libjniopencv_photo.so => facerecognition/src/main/jniLibs/armeabi-v7a/libjniopencv_photo.so
* libs/armeabi-v7a/libjniopencv_stitching.so => facerecognition/src/main/jniLibs/armeabi-v7a/libjniopencv_stitching.so
* libs/armeabi-v7a/libjniopencv_video.so => facerecognition/src/main/jniLibs/armeabi-v7a/libjniopencv_video.so
* libs/armeabi-v7a/libjniopencv_videostab.so => facerecognition/src/main/jniLibs/armeabi-v7a/libjniopencv_videostab.so
* libs/armeabi-v7a/libjnipostproc.so => facerecognition/src/main/jniLibs/armeabi-v7a/libjnipostproc.so
* libs/armeabi-v7a/libjniswresample.so => facerecognition/src/main/jniLibs/armeabi-v7a/libjniswresample.so
* libs/armeabi-v7a/libjniswscale.so => facerecognition/src/main/jniLibs/armeabi-v7a/libjniswscale.so
* libs/armeabi-v7a/libnative_camera_r2.2.0.so => facerecognition/src/main/jniLibs/armeabi-v7a/libnative_camera_r2.2.0.so
* libs/armeabi-v7a/libnative_camera_r2.3.3.so => facerecognition/src/main/jniLibs/armeabi-v7a/libnative_camera_r2.3.3.so
* libs/armeabi-v7a/libnative_camera_r3.0.1.so => facerecognition/src/main/jniLibs/armeabi-v7a/libnative_camera_r3.0.1.so
* libs/armeabi-v7a/libnative_camera_r4.0.0.so => facerecognition/src/main/jniLibs/armeabi-v7a/libnative_camera_r4.0.0.so
* libs/armeabi-v7a/libnative_camera_r4.0.3.so => facerecognition/src/main/jniLibs/armeabi-v7a/libnative_camera_r4.0.3.so
* libs/armeabi-v7a/libopencv_calib3d.so => facerecognition/src/main/jniLibs/armeabi-v7a/libopencv_calib3d.so
* libs/armeabi-v7a/libopencv_contrib.so => facerecognition/src/main/jniLibs/armeabi-v7a/libopencv_contrib.so
* libs/armeabi-v7a/libopencv_core.so => facerecognition/src/main/jniLibs/armeabi-v7a/libopencv_core.so
* libs/armeabi-v7a/libopencv_features2d.so => facerecognition/src/main/jniLibs/armeabi-v7a/libopencv_features2d.so
* libs/armeabi-v7a/libopencv_flann.so => facerecognition/src/main/jniLibs/armeabi-v7a/libopencv_flann.so
* libs/armeabi-v7a/libopencv_highgui.so => facerecognition/src/main/jniLibs/armeabi-v7a/libopencv_highgui.so
* libs/armeabi-v7a/libopencv_imgproc.so => facerecognition/src/main/jniLibs/armeabi-v7a/libopencv_imgproc.so
* libs/armeabi-v7a/libopencv_info.so => facerecognition/src/main/jniLibs/armeabi-v7a/libopencv_info.so
* libs/armeabi-v7a/libopencv_legacy.so => facerecognition/src/main/jniLibs/armeabi-v7a/libopencv_legacy.so
* libs/armeabi-v7a/libopencv_ml.so => facerecognition/src/main/jniLibs/armeabi-v7a/libopencv_ml.so
* libs/armeabi-v7a/libopencv_nonfree.so => facerecognition/src/main/jniLibs/armeabi-v7a/libopencv_nonfree.so
* libs/armeabi-v7a/libopencv_objdetect.so => facerecognition/src/main/jniLibs/armeabi-v7a/libopencv_objdetect.so
* libs/armeabi-v7a/libopencv_photo.so => facerecognition/src/main/jniLibs/armeabi-v7a/libopencv_photo.so
* libs/armeabi-v7a/libopencv_stitching.so => facerecognition/src/main/jniLibs/armeabi-v7a/libopencv_stitching.so
* libs/armeabi-v7a/libopencv_ts.so => facerecognition/src/main/jniLibs/armeabi-v7a/libopencv_ts.so
* libs/armeabi-v7a/libopencv_video.so => facerecognition/src/main/jniLibs/armeabi-v7a/libopencv_video.so
* libs/armeabi-v7a/libopencv_videostab.so => facerecognition/src/main/jniLibs/armeabi-v7a/libopencv_videostab.so
* libs/armeabi-v7a/libtbb.so => facerecognition/src/main/jniLibs/armeabi-v7a/libtbb.so
* libs/javacpp.jar => facerecognition/libs/javacpp.jar
* libs/javacv.jar => facerecognition/libs/javacv.jar
* lint.xml => facerecognition/lint.xml
* res/ => facerecognition/src/main/res/
* src/ => facerecognition/src/main/java/

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
