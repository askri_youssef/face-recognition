package org.opencv.javacv.facerecognition;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


public class SearchActivity extends Activity implements CameraBridgeViewBase.CvCameraViewListener2{

    private Tutorial3View mOpenCvCameraView;
    private CascadeClassifier mJavaDetector;
    private String TAG="Debug";


    private File mCascadeFile;
    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    try {
                        // load cascade file from application resources
                        InputStream is = getResources().openRawResource(R.raw.lbpcascade_frontalface);
                        File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
                        mCascadeFile = new File(cascadeDir, "lbpcascade.xml");
                        FileOutputStream os = new FileOutputStream(mCascadeFile);

                        byte[] buffer = new byte[4096];
                        int bytesRead;
                        while ((bytesRead = is.read(buffer)) != -1) {
                            os.write(buffer, 0, bytesRead);
                        }
                        is.close();
                        os.close();

                        mJavaDetector = new CascadeClassifier(mCascadeFile.getAbsolutePath());
                        if (mJavaDetector.empty()) {
                            Log.e(TAG, "Failed to load cascade classifier");
                            mJavaDetector = null;
                        } else
                            Log.i(TAG, "Loaded cascade classifier from " + mCascadeFile.getAbsolutePath());

                        //                 mNativeDetector = new DetectionBasedTracker(mCascadeFile.getAbsolutePath(), 0);

                        cascadeDir.delete();

                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e(TAG, "Failed to load cascade. Exception thrown: " + e);
                    }
                    mOpenCvCameraView.enableView();

                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };
    private Button swichCam;
    private int frontCam=1;
    private int backCam=2;
    private int mChooseCamera=frontCam;
    private ImageView faceView;
    private Handler mHandler;
    private Bitmap mBitmap;
    private int mLikely;
    private Mat mRgba;
    private Mat mGray;
    private int mAbsoluteFaceSize=0;
    private float mRelativeFaceSize=0.2f;
    public static final int IDLE= 2;
    private int faceState=IDLE;
    private int SEARCHING=1;
    private PersonRecognizer fr;
    private DataBaseHandler db;
    private Person_Recognizer recognizer;
    private Mat m;
    private int idPerson;
    private ImageView viewstat;
    private TextView namePerson;
    private Button trainBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_search);

        mOpenCvCameraView = (Tutorial3View) findViewById(R.id.surface_view);

        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);

        mOpenCvCameraView.setCvCameraViewListener(this);

        swichCam=(Button)findViewById(R.id.statcam);
        swichCam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mChooseCamera == frontCam) {
                    mChooseCamera = backCam;
                    mOpenCvCameraView.setCamBack();
                } else {
                    mChooseCamera = frontCam;
                    mOpenCvCameraView.setCamFront();

                }
            }
        });

        faceView=(ImageView)findViewById(R.id.facev);

        viewstat=(ImageView)findViewById(R.id.viewstat);
        namePerson=(TextView)findViewById(R.id.nameperson);

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.obj=="IMG")
                {
                    Canvas canvas = new Canvas();
                    canvas.setBitmap(mBitmap);
                    faceView.setImageBitmap(mBitmap);
                }
                else if (msg.obj!="Unkown"){

                    String nmPerson=db.getPerson(Integer.parseInt(msg.obj.toString())).getNom();
                    namePerson.setText(nmPerson+" prob="+String.valueOf(mLikely)+"***"+String.valueOf(recognizer.getCounter()));
                    if (mLikely<0);
                    else if (mLikely<80)
                        viewstat.setBackgroundResource(R.drawable.green);
                    else if (mLikely<120)
                        viewstat.setBackgroundResource(R.drawable.pink);
                    else
                        viewstat.setBackgroundResource(R.drawable.red);

                }else {
                    namePerson.setText("Unkown");
                    viewstat.setBackgroundResource(R.drawable.red);
                }

            }
        };

        db=new DataBaseHandler(this);
        recognizer=new Person_Recognizer(new ArrayList<Photo>(db.getPhotoList()));
        Toast.makeText(getApplicationContext(),"Counter="+String.valueOf(recognizer.getCounter()),Toast.LENGTH_LONG).show();

        trainBtn=(Button)findViewById(R.id.train);

        trainBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(SearchActivity.this,TrainActivity.class);
                startActivity(intent);
            }
        });
    }
    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }


    @Override
    public void onCameraViewStarted(int width, int height) {

    }

    @Override
    public void onCameraViewStopped() {

    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        mRgba=inputFrame.rgba();
        mGray=inputFrame.gray();
        //Imgproc.rectangle(mRgba, new Point(50, 50), new Point(100 + 100, 100 + 100), new Scalar(0, 255, 0));
        if (mAbsoluteFaceSize == 0) {
            int height = mGray.rows();
            if (Math.round(height * mRelativeFaceSize) > 0) {
                mAbsoluteFaceSize = Math.round(height * mRelativeFaceSize);
            }
        }
        MatOfRect faces = new MatOfRect();

        if (mJavaDetector != null)
            mJavaDetector.detectMultiScale(mGray, faces, 1.1, 2, 2, // TODO: objdetect.CV_HAAR_SCALE_IMAGE
                    new Size(mAbsoluteFaceSize, mAbsoluteFaceSize), new Size());
        else
            Log.e(TAG, "Detection method is not selected!");

        Rect[] facesArray = faces.toArray();
        if ((facesArray.length>0))
        {
            m=new Mat();
            m=mGray.submat(facesArray[0]);
            mBitmap = Bitmap.createBitmap(m.width(),m.height(), Bitmap.Config.ARGB_8888);

            Utils.matToBitmap(m, mBitmap);
            Message msg = new Message();
            String textTochange = "IMG";
            msg.obj = textTochange;
            mHandler.sendMessage(msg);

            idPerson=recognizer.getIdPerson(m);

            msg=new Message();
            if(idPerson!=-1)
            msg.obj=String.valueOf(idPerson);
            else msg.obj="Unkown";
            mHandler.sendMessage(msg);
            mLikely=recognizer.getProb();
           /* textTochange=fr.predict(m);
            mLikely=fr.getProb();
            msg = new Message();
            msg.obj = textTochange;
            mHandler.sendMessage(msg);*/

        }
        //for (int i = 0; i < facesArray.length; i++)
        //Core.rectangle(mRgba, facesArray[i].tl(), facesArray[i].br(), FACE_RECT_COLOR, 3);
        //return mRgba;
        //Imgproc.rectangle(mRgba, new Point(50,50), new Point(200,200), FACE_RECT_COLOR, 3);

        return mRgba;
    }
}
