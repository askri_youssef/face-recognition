package org.opencv.javacv.facerecognition;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public class TrainActivity extends Activity  implements CameraBridgeViewBase.CvCameraViewListener2, AdapterView.OnItemSelectedListener, ViewSwitcher.ViewFactory {

    private ImageSwitcher mISwitcher;
    private ArrayList<Bitmap> allimages = new ArrayList<Bitmap>();
    private static final String TAG = "Debug==>";

    public static final int TRAINING= 0;
    public static final int SEARCHING= 1;
    public static final int IDLE= 2;

    private static final int frontCam =1;
    private static final int backCam =2;

    private Tutorial3View mOpenCvCameraView;


    private CascadeClassifier mJavaDetector;

    private int exist_id=0;
    private int new_id=1;

    private int verif_id=exist_id;




    private File mCascadeFile;
    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this){
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    try {
                        // load cascade file from application resources
                        InputStream is = getResources().openRawResource(R.raw.lbpcascade_frontalface);
                        File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
                        mCascadeFile = new File(cascadeDir, "lbpcascade.xml");
                        FileOutputStream os = new FileOutputStream(mCascadeFile);

                        byte[] buffer = new byte[4096];
                        int bytesRead;
                        while ((bytesRead = is.read(buffer)) != -1) {
                            os.write(buffer, 0, bytesRead);
                        }
                        is.close();
                        os.close();

                        mJavaDetector = new CascadeClassifier(mCascadeFile.getAbsolutePath());
                        if (mJavaDetector.empty()) {
                            Log.e(TAG, "Failed to load cascade classifier");
                            mJavaDetector = null;
                        } else
                            Log.i(TAG, "Loaded cascade classifier from " + mCascadeFile.getAbsolutePath());

                        //                 mNativeDetector = new DetectionBasedTracker(mCascadeFile.getAbsolutePath(), 0);

                        cascadeDir.delete();

                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e(TAG, "Failed to load cascade. Exception thrown: " + e);
                    }
                    mOpenCvCameraView.enableView();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };
    private Mat mRgba;
    private Mat mGray;
    private int mAbsoluteFaceSize=0;
    private float mRelativeFaceSize=0.2f;
    private int faceState=IDLE;
    private int countImages=0;
    private EditText text;
    private Bitmap mBitmap;
    private Handler mHandler;

    private PersonRecognizer fr;
    private int mLikely=999;
    private Button imCamera;
    private int mChooseCamera=backCam;
    private ImageView faceView;
    private static final Scalar FACE_RECT_COLOR     = new Scalar(0, 255, 0, 255);
    private DataBaseHandler db;
    private Button delete_photo;
    private Button add_photo;
    private EditText id_person;
    private EditText name_person;
    private Gallery gallery;
    private List<Photo> photos;
    private int posPhoto;
    private Button handlerPersonBtn;
    private Button backbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_train);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        mOpenCvCameraView = (Tutorial3View) findViewById(R.id.surface_view_train);

        mOpenCvCameraView.setCvCameraViewListener(this);

        imCamera=(Button)findViewById(R.id.camswitch);

        imCamera.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                if (mChooseCamera == frontCam) {
                    mChooseCamera = backCam;
                    mOpenCvCameraView.setCamBack();
                } else {
                    mChooseCamera = frontCam;
                    mOpenCvCameraView.setCamFront();

                }
            }
        });

//        getImages();

        mISwitcher = (ImageSwitcher)findViewById(R.id.switcher);
        mISwitcher.setFactory(this);
        // some animation when image changes
        mISwitcher.setInAnimation(AnimationUtils.loadAnimation(this,
                android.R.anim.fade_in));
        mISwitcher.setOutAnimation(AnimationUtils.loadAnimation(this,
                android.R.anim.fade_out));

        gallery = (Gallery) findViewById(R.id.gallery);
        gallery.setAdapter(new ImageAdapter(this));
        gallery.setOnItemSelectedListener(this);
        faceView=(ImageView)findViewById(R.id.faceview);

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.obj=="IMG")
                {
                    Canvas canvas = new Canvas();
                    canvas.setBitmap(mBitmap);
                    faceView.setImageBitmap(mBitmap);
                }

            }
        };

        id_person=(EditText)findViewById(R.id.idperson);
        id_person.addTextChangedListener(new IdPersonChangeTxtListener());

        delete_photo=(Button)findViewById(R.id.delphoto);
        delete_photo.setOnClickListener(new DeletePhotoListener());

        add_photo=(Button)findViewById(R.id.addphoto);
        add_photo.setOnClickListener(new AddPhotoListener());

        name_person=(EditText)findViewById(R.id.nameperson);

        handlerPersonBtn=(Button)findViewById(R.id.add_person);


        db = new DataBaseHandler(this);
        int n=db.getPhotoCount();
        Toast.makeText(getApplicationContext(),"count="+String.valueOf(n),Toast.LENGTH_LONG).show();

        backbtn=(Button)findViewById(R.id.back);

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(TrainActivity.this,SearchActivity.class);
                startActivity(intent);
            }
        });
    }

    private void getImages() {

        /*allimages.add(this.getResources().getDrawable(R.drawable.lightbulb));
        allimages.add(this.getResources().getDrawable(R.drawable.ic_red));
        allimages.add(this.getResources().getDrawable(R.drawable.ic_yellow));*/
        Person person=new Person(getIdPersonEditTxt(),name_person.getText().toString());
        photos=db.getPhotosOfPerson(person);
//        photos=db.getPhotoList();
        Toast.makeText(getApplicationContext(),"Count= "+String.valueOf(photos.size()),Toast.LENGTH_LONG).show();
        allimages.clear();
        for(Photo photo:photos){
            allimages.add(photo.getImg());
        }
    }

    private void refresh(){
        getImages();
        gallery.setAdapter(new ImageAdapter(this));

    }

    private void clearGallery(){
        allimages.clear();
        gallery.setAdapter(new ImageAdapter(this));
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        posPhoto=position;
        mISwitcher.setImageDrawable(new BitmapDrawable(getResources(), allimages.get(position)));
//        name_person.setText(String.valueOf(photos.get(position).getId_person()));

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public View makeView() {
        ImageView i = new ImageView(this);
        i.setBackgroundColor(0xFF58A5D1);
        i.setScaleType(ImageView.ScaleType.FIT_CENTER);
        i.setLayoutParams(new ImageSwitcher.LayoutParams(ImageSwitcher.LayoutParams.FILL_PARENT, ImageSwitcher.LayoutParams.FILL_PARENT));
        return i;
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);


    }

    public void onDestroy() {
        super.onDestroy();
        mOpenCvCameraView.disableView();
    }

    @Override
    public void onCameraViewStarted(int width, int height) {


    }

    @Override
    public void onCameraViewStopped() {

    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        mRgba=inputFrame.rgba();
        mGray=inputFrame.gray();
        //Imgproc.rectangle(mRgba, new Point(50, 50), new Point(100 + 100, 100 + 100), new Scalar(0, 255, 0));
        if (mAbsoluteFaceSize == 0) {
            int height = mGray.rows();
            if (Math.round(height * mRelativeFaceSize) > 0) {
                mAbsoluteFaceSize = Math.round(height * mRelativeFaceSize);
            }
        }
        MatOfRect faces = new MatOfRect();

        if (mJavaDetector != null)
            mJavaDetector.detectMultiScale(mGray, faces, 1.1, 2, 2, // TODO: objdetect.CV_HAAR_SCALE_IMAGE
                    new Size(mAbsoluteFaceSize, mAbsoluteFaceSize), new Size());
        else
            Log.e(TAG, "Detection method is not selected!");

        Rect[] facesArray = faces.toArray();

        if ((facesArray.length==1)/*&&
                (faceState==TRAINING)&&
                (!text.getText().toString().isEmpty())*/){

            Mat m=new Mat();
            Rect r=facesArray[0];


            m=mRgba.submat(r);
            mBitmap = Bitmap.createBitmap(m.width(), m.height(), Bitmap.Config.ARGB_8888);


            Utils.matToBitmap(m, mBitmap);

            Message msg = new Message();
            String textTochange = "IMG";
            msg.obj = textTochange;
            mHandler.sendMessage(msg);

            //fr.add(m, text.getText().toString());

        }

        //for (int i = 0; i < facesArray.length; i++)
        //Core.rectangle(mRgba, facesArray[i].tl(), facesArray[i].br(), FACE_RECT_COLOR, 3);
        //return mRgba;
        //Imgproc.rectangle(mRgba, new Point(50,50), new Point(200,200), FACE_RECT_COLOR, 3);

        return mRgba;
    }

    public class ImageAdapter extends BaseAdapter {
        private Context mContext;

        public ImageAdapter(Context c) {
            mContext = c;
        }

        public int getCount() {
            return allimages.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView galleryview = new ImageView(mContext);
            galleryview.setImageDrawable(new BitmapDrawable(allimages.get(position)));
            galleryview.setAdjustViewBounds(true);
            galleryview.setLayoutParams(new Gallery.LayoutParams(Gallery.LayoutParams.WRAP_CONTENT, Gallery.LayoutParams.WRAP_CONTENT));
            galleryview.setPadding(5, 0, 5, 0);
            galleryview.setBackgroundResource(android.R.drawable.picture_frame);
            return galleryview;
        }
    }

    private class DeletePhotoListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            db.deletePhoto(photos.get(posPhoto));
            refresh();
        }
    }

    private class AddPhotoListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Photo photo=new Photo(mBitmap,getIdPersonEditTxt());
            db.addPhoto(photo);
            refresh();
        }
    }



    private class IdPersonChangeTxtListener implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            if (id_person.getText().toString().isEmpty()){
                add_photo.setVisibility(View.INVISIBLE);
            }else add_photo.setVisibility(View.VISIBLE);
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            Toast.makeText(getApplicationContext(),"in EditText "+s.toString(),Toast.LENGTH_LONG).show();
            Person person = null;
            if (id_person.getText().toString().isEmpty()){
                add_photo.setVisibility(View.INVISIBLE);
                handlerPersonBtn.setVisibility(View.INVISIBLE);
                name_person.setText("NAME");
                clearGallery();
                ColorDrawable cd = new ColorDrawable(R.color.bgcolor);
                cd.setAlpha(255);
                mISwitcher.setImageDrawable(cd);
            }else {
                refresh();
                add_photo.setVisibility(View.VISIBLE);
                person = db.getPerson(Integer.parseInt(s.toString()));
                if(person!=null){
                    name_person.setText(person.getNom());
                    handlerPersonBtn.setVisibility(View.VISIBLE);
                    handlerPersonBtn.setText("Edit Person");
                    handlerPersonBtn.setOnClickListener(new EditPersonListener());

                }else {
                    name_person.setText("NAME");
                    handlerPersonBtn.setVisibility(View.VISIBLE);
                    handlerPersonBtn.setText("Add Person");
                    handlerPersonBtn.setOnClickListener(new AddPersonListener());
                }
            }
        }
    }

    private int getIdPersonEditTxt(){
        return Integer.parseInt(id_person.getText().toString());
    }

    private class EditPersonListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Person person=new Person(getIdPersonEditTxt(),name_person.getText().toString());
            db.updatePerson(person);
        }
    }

    private class AddPersonListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Person person=new Person(getIdPersonEditTxt(),name_person.getText().toString());
            db.addPerson(person);
            handlerPersonBtn.setText("Edit Person");
        }
    }
}
