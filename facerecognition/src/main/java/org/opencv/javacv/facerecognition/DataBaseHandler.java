package org.opencv.javacv.facerecognition;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 10/07/15.
 */
public class DataBaseHandler extends SQLiteOpenHelper{

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "dbFaceID";

    // Person table name
    private static final String TABLE_PERSON = "Person";

    // Person Table Columns names
    private static final String ID_PERSON = "id";
    private static final String NAME_PERSON = "name";

    // Photo table name
    private static final String TABLE_PHOTO = "Photo";

    // Person Table Columns names
    private static final String ID_PHOTO = "id_img";
    private static final String IMG = "img";
    private static final String ID_P = "id";
    private Photo photo;
    private Person person;


    public DataBaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("PRAGMA foreign_keys = ON;");

        String CREATE_TABLE_PERSON="CREATE TABLE "+TABLE_PERSON+" ("
                +ID_PERSON+"  integer primary key , "+NAME_PERSON+" text)";

        String CREATE_TABLE_PHOTO="CREATE TABLE "+TABLE_PHOTO+"("
                +ID_PHOTO+"  integer primary key autoincrement, "+IMG+" blob ,"
                +ID_P+" INTEGER NOT NULL , "
                + " FOREIGN KEY('"+ID_P+"') REFERENCES "+TABLE_PERSON+" ("+ID_PERSON+") ON DELETE CASCADE ) ;";


        db.execSQL(CREATE_TABLE_PERSON);
        db.execSQL(CREATE_TABLE_PHOTO);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PERSON);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PHOTO);

        // Create tables again
        onCreate(db);

    }

    public void upgrade(){
        this.onUpgrade(getWritableDatabase(), DATABASE_VERSION, DATABASE_VERSION);
    }
    //*************************Person HANDLER***********************
    // Adding new Person
    public void addPerson(Person person) {
        SQLiteDatabase db=this.getWritableDatabase();

        ContentValues values=new ContentValues();
        values.put(ID_PERSON,person.getId());
        values.put(NAME_PERSON, person.getNom());

        db.insert(TABLE_PERSON, null, values);
        db.close();
    }

    // Getting single Person
    public Person getPerson(int id) {
        SQLiteDatabase db=this.getWritableDatabase();

        Cursor cursor=db.query(TABLE_PERSON,new String[]{ID_PERSON,
                NAME_PERSON},ID_PERSON+"=?",
                new String[]{String.valueOf(id)},null,null,null,null);
        if(cursor.getCount() >= 1) {
            while (cursor.moveToNext()) {
                cursor.moveToFirst();
                Person person = new Person(id
                        , cursor.getString(1));

                db.close();
                return person;
            }
        }
        return null;
    }

    // Getting All Person
    public List<Person> getAllPerson() {
        List<Person> personList=new ArrayList<Person>();

        String selectQuery="SELECT * FROM "+TABLE_PERSON;

        SQLiteDatabase db=this.getWritableDatabase();

        Cursor cursor=db.rawQuery(selectQuery,null);

        while (cursor.moveToNext()){
            Person person=new Person(Integer.parseInt(cursor.getString(0))
                    ,cursor.getString(1));
            personList.add(person);
        }
        cursor.close();
        db.close();
        return personList;
    }

    // Getting Person Count
    public int getPersonCount() {

        String selectQuery="SELECT * FROM "+TABLE_PERSON;

        SQLiteDatabase db=this.getWritableDatabase();

        Cursor cursor=db.rawQuery(selectQuery,null);
        cursor.close();
        db.close();
        return cursor.getColumnCount();
    }

    // Updating single Person
    public int updatePerson(Person person) {
        SQLiteDatabase db=this.getWritableDatabase();

        ContentValues values=new ContentValues();
        values.put(ID_PERSON, person.getId());
        values.put(NAME_PERSON, person.getNom());

        return db.update(TABLE_PERSON, values, ID_PERSON + "=?", new String[]{String.valueOf(person.getId())});
    }

    // Deleting single Person
    public void deletePerson(Person person) {
        SQLiteDatabase db=this.getWritableDatabase();

        db.delete(TABLE_PERSON, ID_PERSON + "=?", new String[]{String.valueOf(person.getId())});
        db.close();
    }

    //***************************Photo Handler**********************
    public void addPhoto(Photo photo){
        SQLiteDatabase db=this.getWritableDatabase();

        ContentValues values=new ContentValues();
        values.put(IMG,Photo.getBytes(photo.getImg()));
        values.put(ID_P, photo.getId_person());

        db.insert(TABLE_PHOTO, null, values);
        db.close();
    }

    public Photo getPhoto(int id_photo){
        SQLiteDatabase db=getWritableDatabase();

        Cursor cursor=db.query(TABLE_PHOTO,new String[]{ID_PHOTO,IMG,ID_P},
                ID_PHOTO+"=?",new String[]{String.valueOf(id_photo)},null,null,null);

        if (cursor!=null)
            cursor.moveToFirst();
        Photo photo=new Photo(Integer.parseInt(cursor.getString(0)),
                Photo.getImageByte(cursor.getBlob(1)),
                Integer.parseInt(cursor.getString(2)));

        return photo;
    }

    public List<Photo> getPhotoList(){
        List<Photo> photoList=new ArrayList<Photo>();
        SQLiteDatabase db=getWritableDatabase();

        String selectQuery="SELECT * FROM "+TABLE_PHOTO;

        Cursor cursor=db.rawQuery(selectQuery, null);

        while (cursor.moveToNext()){
            Photo photo=new Photo(Integer.parseInt(cursor.getString(0)),
                    Photo.getImageByte(cursor.getBlob(1)),
                    Integer.parseInt(cursor.getString(2)));
            photoList.add(photo);
        }
        return photoList;
    }

    public List<Photo> getPhotosOfPerson(Person person){
        List<Photo> photoList=new ArrayList<Photo>();
        SQLiteDatabase db=getWritableDatabase();

        Cursor cursor=db.query(TABLE_PHOTO,new String[]{ID_PHOTO,
                        IMG,ID_P},ID_P+"=?",
                new String[]{String.valueOf(person.getId())},null,null,null,null);
        if(cursor.getCount() >= 1) {
            while (cursor.moveToNext()) {
                Photo photo=new Photo(Integer.parseInt(cursor.getString(0)),
                        Photo.getImageByte(cursor.getBlob(1)),
                        Integer.parseInt(cursor.getString(2)));
                photoList.add(photo);
            }
        }
        return photoList;
    }

    public int getPhotoCount(){

        String selectQuery="SELECT * FROM "+TABLE_PHOTO;

        SQLiteDatabase db=this.getWritableDatabase();

        Cursor cursor=db.rawQuery(selectQuery,null);
        //cursor.close();
        db.close();
        return cursor.getColumnCount();
    }

    public int updatePhoto(Photo photo){
        SQLiteDatabase db=this.getWritableDatabase();

        ContentValues values=new ContentValues();
        values.put(IMG, Photo.getBytes(photo.getImg()));
        values.put(ID_P,photo.getId_person());

        return db.update(TABLE_PHOTO, values, ID_PHOTO + "=?", new String[]{String.valueOf(photo.getId())});
    }

    public void deletePhoto(Photo photo){
        SQLiteDatabase db=this.getWritableDatabase();

        db.delete(TABLE_PHOTO, ID_PHOTO + "=?", new String[]{String.valueOf(photo.getId())});
        db.close();}
}

