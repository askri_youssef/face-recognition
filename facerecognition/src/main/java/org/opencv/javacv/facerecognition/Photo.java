package org.opencv.javacv.facerecognition;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;

/**
 * Created by root on 10/07/15.
 */
public class Photo {
    int id;
    Bitmap img;
    int id_person;

    Photo(int id,Bitmap img,int id_person){
        this.id=id;
        this.img=img;
        this.id_person=id_person;
    }

    Photo(Bitmap img,int id_person){
        this.img=img;
        this.id_person=id_person;
    }
    public void setId(int id) {
        this.id = id;
    }

    public void setImg(Bitmap img) {
        this.img = img;
    }

    public void setId_person(int id_person) {
        this.id_person = id_person;
    }

    public int getId() {
        return id;
    }

    public Bitmap getImg() {
        return img;
    }

    public int getId_person() {
        return id_person;
    }

    // convert from bitmap to byte array
    public static byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        return stream.toByteArray();
    }

    // convert from byte array to bitmap
    public static Bitmap getImageByte(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }
}
