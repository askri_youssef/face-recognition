package org.opencv.javacv.facerecognition;

import android.graphics.Bitmap;
import android.widget.Toast;

import com.googlecode.javacv.cpp.opencv_contrib;
import com.googlecode.javacv.cpp.opencv_core;
import com.googlecode.javacv.cpp.opencv_imgproc;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.photo.Tonemap;

import java.util.List;

import static com.googlecode.javacv.cpp.opencv_core.IPL_DEPTH_8U;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvCvtColor;

/**
 * Created by root on 03/08/15.
 */
public class Person_Recognizer {

    private final List<Photo> photos;
    private int[] labelss;
    private opencv_core.IplImage grayImg;
    private int counter=0;
    opencv_contrib.FaceRecognizer faceRecognizer;
    private Mat mat;
    private opencv_core.MatVector matVector;

    static  final int WIDTH= 165;
    static  final int HEIGHT= 165;
    private int mProb;

    public Person_Recognizer(List<Photo> photos){
        faceRecognizer =  com.googlecode.javacv.cpp.opencv_contrib.createLBPHFaceRecognizer(2,8,8,8,200);
        this.photos=photos;
        labelss=new int[photos.size()];
        matVector=new opencv_core.MatVector(photos.size());
        for (Photo photo:photos) {
            grayImg=BitmapToIplImage(photo.getImg(),photo.getImg().getWidth(),photo.getImg().getWidth());
            matVector.put(counter,grayImg);
            labelss[counter]=photo.getId_person();
            counter++;
        }
        if(counter>1)
        faceRecognizer.train(matVector,labelss);

    }

    int getCounter(){return counter;}
    int getIdPerson(Mat mat){
        this.mat=mat;
        int n[] = new int[1];
        double p[] = new double[1];
        opencv_core.IplImage ipl = MatToIplImage(mat,WIDTH, HEIGHT);;

        faceRecognizer.predict(ipl, n, p);


        if (n[0]!=-1)
            mProb=(int)p[0];
        else
            mProb=-1;
        //	if ((n[0] != -1)&&(p[0]<95))
        if (n[0] != -1)
            return n[0];
        else
            return -1;
    }

    int getProb(){
        return mProb;
    }

    opencv_core.IplImage BitmapToIplImage(Bitmap bmp, int width, int height) {

        if ((width != -1) || (height != -1)) {
            Bitmap bmp2 = Bitmap.createScaledBitmap(bmp, width, height, false);
            bmp = bmp2;
        }

        opencv_core.IplImage image = opencv_core.IplImage.create(bmp.getWidth(), bmp.getHeight(),
                IPL_DEPTH_8U, 4);

        bmp.copyPixelsToBuffer(image.getByteBuffer());

        opencv_core.IplImage grayImg = opencv_core.IplImage.create(image.width(), image.height(),
                IPL_DEPTH_8U, 1);

        cvCvtColor(image, grayImg, opencv_imgproc.CV_BGR2GRAY);

        return grayImg;
    }


    opencv_core.IplImage MatToIplImage(Mat m,int width,int heigth)
    {


        Bitmap bmp=Bitmap.createBitmap(m.width(), m.height(), Bitmap.Config.ARGB_8888);


        Utils.matToBitmap(m, bmp);
        return BitmapToIplImage(bmp,width, heigth);

    }
}
