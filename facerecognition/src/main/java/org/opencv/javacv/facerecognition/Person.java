package org.opencv.javacv.facerecognition;

import android.graphics.Bitmap;

import java.util.List;

/**
 * Created by root on 10/07/15.
 */
public class Person {
    private int id;
    private String nom;

    Person(int id,String nom){
        this.id=id;
        this.nom=nom;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }


    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

}
